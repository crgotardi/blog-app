const assert = require('assert');
const Article = require('../blog-app/models/article');

describe('saving records', function(done){
    it("saving records to the database", function(){
        let article = new Article({
            title: 'Example title',
            content: 'example content',
            author: 'cristiano',
        });

        article.save().then(function(){
            assert(article.isNew === false);
            done();
        });
    });
})