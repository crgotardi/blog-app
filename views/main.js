var React = require('react');
import Header from './partials/header';
import WidthButton from './partials/button';
import FilesSection from './partials/files-section';

class Layout extends React.Component {
  render() {
    return (
      <html>
        <Header></Header>
        <main>
          <div className="sidebar">
            <div className='sidebar-toggle'>
            <div className='sidebar-top'>
            <div className='logo-section'>
              <img src="http://localhost:3000/public/img/logo.jpg"></img>
            </div>
            <WidthButton></WidthButton>
            <FilesSection></FilesSection>
            <br></br>
            <span className="filesSectionTitle">Categorias</span>
            <ul className="filesSectionList">
              <li className="filesSectionItem">
              {this.props.data}
              </li>
            </ul>
          </div>
          <div className="sidebar-bottom">
            <div className='login-section'>
              <img src="http://localhost:3000/public/img/logo-bottom.jpg"></img>
              <div className='user'>
                <p><strong>Rudi Duarte</strong></p>
                <p>Usuário Premium</p>
              </div>
              <img src="http://localhost:3000/public/img/logout-icon.jpg"></img>
            </div>
          </div>
            </div>
        </div>
          <div className="options">
            <h1>Meus artigos</h1>
            <div className='search-section'>
              <div className="filter-section">
                <img className="filter-icon" src="http://localhost:3000/public/img/filter-icon.jpg"></img>
              </div>
              <input type="search" className="searchBar" placeholder="Mais novos Primeiro"></input>
              <div className="clear-section">
                <img className="clear-icon" src="http://localhost:3000/public/img/clear-icon.jpg"></img>
              </div>
            </div>
             
            <hr></hr>
            <div className="select">
              <span>0 artigos selecionados</span>
              <div className="controls">
                <img src="http://localhost:3000/public/img/share-icon.jpg"></img>
                <img src="http://localhost:3000/public/img/edit-icon.jpg"></img>
                <img src="http://localhost:3000/public/img/trash-icon.jpg"></img>
              </div>
            </div>
            <div className="articlesList">
              <div className="article">
                <div className="selectIcon">
                  <img src="http://localhost:3000/public/img/select-inative-icon.jpg"></img>
                </div>
                <div className="articleData">
                  <p>Rudi Duarte</p>
                  <p className="strongText">O que é identidade visual</p>
                  <p>data</p>
                </div>
              </div>
              <div className="article">
                <div className="selectIcon">
                  <img src="http://localhost:3000/public/img/select-inative-icon.jpg"></img>
                </div>
                <div className="articleData">
                  <p>Rudi Duarte</p>
                  <p className="strongText">O que é identidade visual</p>
                  <p>data</p>
                </div>
              </div>
              <div className="article">
                <div className="selectIcon">
                  <img src="http://localhost:3000/public/img/select-inative-icon.jpg"></img>
                </div>
                <div className="articleData">
                  <p>Rudi Duarte</p>
                  <p className="strongText">O que é identidade visual</p>
                  <p>data</p>
                </div>
              </div>
              <div className="article-active">
                <div className="selectIcon">
                  <img src="http://localhost:3000/public/img/select-inative-icon.jpg"></img>
                </div>
                <div className="articleData">
                  <p>Rudi Duarte</p>
                  <p className="strongText">O que é identidade visual</p>
                  <p>data</p>
                </div>
              </div>
              <div className="article">
                <div className="selectIcon">
                  <img src="http://localhost:3000/public/img/select-inative-icon.jpg"></img>
                </div>
                <div className="articleData">
                  <p>Rudi Duarte</p>
                  <p className="strongText">O que é identidade visual</p>
                  <p>data</p>
                </div>
              </div>
              <div className="article">
                <div className="selectIcon">
                  <img src="http://localhost:3000/public/img/select-inative-icon.jpg"></img>
                </div>
                <div className="articleData">
                  <p>Rudi Duarte</p>
                  <p className="strongText">O que é identidade visual</p>
                  <p>data</p>
                </div>
              </div>
            </div>
            <div className="addSection">
              <img src="http://localhost:3000/public/img/add-button.jpg" className="addButton"></img>
            </div>
          </div>
          <div className="view">
            <div className="viewHeader">
              <h1>Titulo do artigo</h1>
              <div className="infoHeader">
                <p>Publicado por: Autor</p>
                <p>data</p>
              </div>
              <br></br>
              <img src="http://localhost:3000/public/img/header-img.jpg"></img>
              <div className="viewContent">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi vitae neque dolor. Vivamus aliquet magna eget ultrices varius. Quisque rutrum justo a viverra ultrices. Vestibulum posuere pretium enim vitae auctor. Vestibulum sodales eros odio. Quisque luctus luctus consequat. Sed turpis ligula, iaculis eu venenatis eget, mattis ac dolor. Fusce a congue neque.</p> 
                
                <p>Vivamus at nulla a lacus malesuada facilisis nec eget dui. Vivamus suscipit diam sed turpis fermentum imperdiet. Mauris at diam venenatis, luctus eros et, rutrum eros. Nam scelerisque porttitor velit, in venenatis ipsum. Praesent magna enim, bibendum sit amet magna ac, commodo cursus velit. Duis tristique iaculis orci non suscipit. Integer placerat diam urna, nec porttitor magna accumsan sit amet. Nam facilisis suscipit nisl vitae condimentum.</p>
              </div>
            </div>
          </div>
        </main>
      </html>
    );
  }
}

module.exports = Layout;