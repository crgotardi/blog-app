var React = require('react');

class FilesSection extends React.Component {
 handleClick(){
   console.log('clicked')
 }
 
  render() {
    return (
        <div>
          <span className="filesSectionTitle">Arquivos</span>
          <ul className="filesSectionList">
            <li className="filesSectionItem" onClick="handleClick()">Meus artigos</li>
            <li className="filesSectionItem">Compartilhados</li>
            <li className="filesSectionItem">Lixeira</li>
          </ul>
        </div>
    );
  }
}

module.exports = FilesSection;