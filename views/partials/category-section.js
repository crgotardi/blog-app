var React = require('react');

class CategorySection extends React.Component {
  render() {
    return (
        <div>
          <span className="filesSectionTitle">Categorias</span>
          <ul className="filesSectionList">
            <li className="filesSectionItem">
              {this.props.data}
            </li>
          </ul>
        </div>
    );
  }
}

module.exports = CategorySection;