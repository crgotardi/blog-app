var React = require('react');

class Header extends React.Component {
  render() {
    return (
        <head>
            <meta charSet="UTF-8"></meta>
            <meta name="viewport" content="width=device-width, initial-scale=1.0"></meta>
            <meta httpEquiv="X-UA-Compatible" content="ie=edge"></meta>
            <link rel="stylesheet" href="/public/css/app.css"></link>
            <title>Document</title>
        </head>
    );
  }
}

module.exports = Header;