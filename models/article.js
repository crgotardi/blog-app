const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// create article schema and model
const ArticleSchema = new Schema({
    title: {
        type: String,
        required: [true, 'Name field is required'],
    },
    content: {
        type: String,
    },
    date: {
        type: Date,
        default: Date.now()
    },
    category: {
        type: String
    }
});
const Article = mongoose.model('article', ArticleSchema);

const UserSchema = new Schema({
    name: {
        type: String,
        required: [true, 'name field is required']
    },
    email: {
        type: String
    },
    password: {
        type: String
    },
    articles: [ArticleSchema]
})

const User = mongoose.model('User', UserSchema);

module.exports = Article;