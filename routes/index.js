const Article = require('../models/article');

exports.index = function(req, res){
    Article.distinct('title', function(err, data){
        if(err){
            console.log(err);
        } else {
           res.render('main', {data: data});
        };
    });
};