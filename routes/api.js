const express = require('express');
const router = express.Router();
const Article = require('../models/article');

router.get("/articles", (req, res) => {
    console.log('articles page');
});

router.post("/articles", (req, res) => {
    Article.create(req.body).then(function(article){
        res.send(article);
    }).catch(next);
});

router.put("/articles/:id", (req, res) => {
    Article.findByIdAndUpdate({_id: req.params.id}, req.body).then(function(){
        Article.findOne({_id: req.params.id}).then(function(article){
            res.send(article);
        })
    })
});

router.delete("/articles/:id", (req, res) => {
    Ninja.findByIdAndRemove({_id: req.params.id}).then(function(article){
        res.send(article);
    })
    res.send({type: 'DELETE'})
});

module.exports = router;