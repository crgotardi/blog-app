const express       = require('express');
const bodyParser    = require('body-parser');
const mongoose      = require('mongoose');
const app           = express();
const { createEngine } = require('express-react-views');

mongoose.connect('mongodb://localhost/blog');

app.use(bodyParser.urlencoded({extended: true}));
app.use('/public', express.static('public'));
//app.use('/api', require('./routes/api'));

app.set('view engine', 'js');
app.engine('js', createEngine());

app.get('/', require('./routes').index);

const port = process.env.PORT || 3000;
app.listen(port, () => console.log(`Server started on port ${port}`));